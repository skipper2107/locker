# Simple transaction manager

## Usage

Extend [UniOfWork](src/UnitOfWork.php) to make your own transaction for certain entity. Just 
pass your work to `carry` method. Instance of [Locker](src/Locker.php) should take instance
of [DriverInterface](src/DriverInterface.php) to manage lock-keys. It's our 
repositories. Write your own or use my [Redis](src/Drivers/Redis.php)-one

## Next steps

* Tests!!!
* Memcached driver
* PDO (in_memory) driver