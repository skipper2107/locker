<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 2/15/18
 * Time: 2:50 PM
 */

namespace Skipper\Locker;

interface DriverInterface
{
    /**
     * @param string $key
     * @param bool $blockOnBusy Would the function be blocked until lock is acquired?
     * @return bool
     */
    public function doLock(string $key, bool $blockOnBusy): bool;

    /**
     * @param string $key
     * @return bool
     */
    public function doRelease(string $key): bool;

    /**
     * @param $key
     * @return bool
     */
    public function haveKeyInDb(string $key): bool;

    /**
     * @param int $time
     * @return DriverInterface
     */
    public function setMaxTimeLiveInDb(int $time): DriverInterface;

    /**
     * @param int $time
     * @return DriverInterface
     */
    public function setLockExpireTimeout(int $time): DriverInterface;
}