<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 2/15/18
 * Time: 3:18 PM
 */

namespace Skipper\Locker;

abstract class UnitOfWork
{
    public const CARRY_STATUS_RESTRICTED = 0;
    public const CARRY_STATUS_FAILED = -1;
    public const CARRY_STATUS_DONE = 1;

    /** @var string $storageKey */
    protected $storageKey;
    /** @var Locker $locker */
    protected $locker;
    /** @var bool $inTransaction */
    protected $inTransaction = false;

    /**
     * UnitOfWork constructor.
     * @param string $key
     * @param Locker $locker
     */
    public function __construct(string $key, Locker $locker)
    {
        $this->generateStorageKey($key);
        $this->locker = $locker;
    }

    /**
     * @param string $key
     */
    protected function generateStorageKey(string $key): void
    {
        $this->storageKey = sprintf('%s:%s', $this->getKeyPrefix(), $key);
    }

    /**
     * @return string
     */
    abstract protected function getKeyPrefix(): string;

    public function __destruct()
    {
        // rollback if not committed
        $this->rollback();
    }

    /**
     * @return UnitOfWork
     */
    public function rollback(): UnitOfWork
    {
        if (false === $this->inTransaction) {
            return $this;
        }
        $this->locker->release($this->storageKey);
        $this->rollbackTransaction();
        $this->inTransaction = false;

        return $this;
    }

    /**
     * Rollback transaction
     */
    abstract protected function rollbackTransaction(): void;

    /**
     * @param callable $work
     * @param array $params
     * @return int
     */
    public function carry(callable $work, array $params = []): int
    {
        $status = self::CARRY_STATUS_FAILED;
        if ($this->begin()->isInTransaction()) {
            try {
                call_user_func($work, ...$params);
                $this->commit();
                $status = self::CARRY_STATUS_DONE;
            } catch (\Exception $e) {
                $this->rollback();
            }
        } else {
            $status = self::CARRY_STATUS_RESTRICTED;
        }

        return $status;
    }

    /**
     * @return bool
     */
    public function isInTransaction(): bool
    {
        return $this->inTransaction;
    }

    /**
     * @return $this
     */
    public function begin(): UnitOfWork
    {
        if (false === $this->locker->lock($this->storageKey)) {
            $this->inTransaction = false;
            return $this;
        }
        $this->beginTransaction();
        $this->inTransaction = true;

        return $this;
    }

    /**
     * Begin transaction
     */
    abstract protected function beginTransaction(): void;

    /**
     * @return UnitOfWork
     */
    public function commit(): UnitOfWork
    {
        if (false === $this->inTransaction) {
            return $this;
        }
        $this->commitTransaction();
        $this->locker->release($this->storageKey);
        $this->inTransaction = false;

        return $this;
    }

    /**
     * Commit transaction
     */
    abstract protected function commitTransaction(): void;
}