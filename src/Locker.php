<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 2/15/18
 * Time: 2:55 PM
 */

namespace Skipper\Locker;

class Locker
{
    /** @var DriverInterface $driver */
    protected $driver;

    /**
     * Locker constructor.
     * @param DriverInterface $driver
     */
    public function __construct(DriverInterface $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Lock record
     * @param string $key
     * @param bool $waitForAvailable
     * @return bool
     */
    public function lock(string $key, bool $waitForAvailable = true): bool
    {
        return $this->driver->doLock($key, $waitForAvailable);
    }

    /**
     * Release record
     * @param string $key
     * @return bool
     */
    public function release(string $key): bool
    {
        return $this->driver->doRelease($key);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function haveKeyInDb(string $key): bool
    {
        return $this->driver->haveKeyInDb($key);
    }

    /**
     * @param $time int
     * @return Locker
     */
    public function setMaxTimeLiveInDb(int $time): Locker
    {
        $this->driver->setMaxTimeLiveInDb($time);

        return $this;
    }

    /**
     * @param $time int
     * @return Locker
     */
    public function setLockExpireTimeout(int $time): Locker
    {
        $this->driver->setLockExpireTimeout($time);

        return $this;
    }
}