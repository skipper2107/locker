<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 2/15/18
 * Time: 4:01 PM
 */

namespace Skipper\Locker\Drivers;

use Predis\Client;
use RedisException;
use Skipper\Locker\DriverInterface;

class Redis implements DriverInterface
{
    const LOCK_ACQUIRE_TIMEOUT = 30;

    /** @var $lockTime int */
    protected $lockTime = 30;
    /** @var $maxLifetime int */
    protected $maxLifetime = 24 * 3600;
    /** @var Client $redis */
    protected $redis;
    /** @var array $transaction */
    protected $transaction = [];

    public function __construct(Client $client)
    {
        $this->redis = $client;
    }

    /**
     * @param string $key
     * @param bool $blockOnBusy Would the function be blocked until lock is acquired?
     * @return bool
     */
    public function doLock(string $key, bool $blockOnBusy): bool
    {
        try {
            $start = time();
            $lockAcquireTimeout = $blockOnBusy ? self::LOCK_ACQUIRE_TIMEOUT : 0;
            do {
                $this->transaction[$key] = $this->timeout();
                $acquired = $this->redis->setnx($key, $this->transaction[$key]);
                if (true === (bool)$acquired) {
                    $this->redis->expire($key, $this->maxLifetime);
                    break;
                }
                $acquired = $this->recover($key);
                if (true === $acquired) {
                    break;
                }
                if ($lockAcquireTimeout === 0) {
                    break;
                }
                usleep(1000000);
            } while (time() < $start + $lockAcquireTimeout);
            return $acquired;
        } catch (RedisException $e) {
            return false;
        }
    }

    /**
     * @return int
     */
    protected function timeout(): int
    {
        return time() + $this->lockTime + 1;
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function recover(string $key): bool
    {
        if ($this->haveKeyInDb($key)) {
            return false;
        }
        $timeout = $this->timeout();
        $this->redis->set($key, $timeout);
        $this->redis->expire($key, $this->maxLifetime);
        $this->transaction[$key] = $timeout;

        return true;
    }

    /**
     * @param $key
     * @return bool
     */
    public function haveKeyInDb(string $key): bool
    {
        return $this->redis->get($key) > time();
    }

    /**
     * @param string $key
     * @return bool
     */
    public function doRelease(string $key): bool
    {
        try {
            if ($this->transaction[$key] > time()) {
                $this->redis->del([$key]);
            }
        } catch (RedisException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param int $time
     * @return DriverInterface
     */
    public function setMaxTimeLiveInDb(int $time): DriverInterface
    {
        $this->maxLifetime = $time;

        return $this;
    }

    /**
     * @param int $time
     * @return DriverInterface
     */
    public function setLockExpireTimeout(int $time): DriverInterface
    {
        $this->lockTime = $time;

        return $this;
    }
}